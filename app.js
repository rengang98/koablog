const koa = require('koa')
const koaRouter = require('koa-router')
const mongoose = require('mongoose')
const db = require('./config/keys').url
const bodyParser = require('koa-bodyparser')
//验证token的辅助npm
const passport = require('koa-passport')

//实例化koa router
const router = new koaRouter()
const app = new koa()

//bodyparser用于获取请求参数 ctx.request.xxx
app.use(bodyParser())

//引入users.js
const users = require('./routes/api/users')
//引入profile.js
const profile = require('./routes/api/profile')
//引入post.js
const posts = require('./routes/api/posts')
//配置路由地址
router.use('/api/users',users)
router.use('/api/profile',profile)
router.use('/api/posts',posts)
//配置路由
app.use(router.routes()).use(router.allowedMethods())
//process.env.PORT是在heroku中获取的端口
const port = process.env.PORT || 5000

//路由
router.get('/', async ctx => {
    ctx.body = {msg: "welcome to blog back-end koa project"}
})

app.use(passport.initialize())
app.use(passport.session())

//回调到config文件中 passport.js
require('./config/passport')(passport)

//连接数据库
mongoose.connect(db,{
    useUnifiedTopology: true,
    useNewUrlParser: true
})
    .then(()=> {
        console.log('mongoDB connected')
    }).catch(err=>{
    console.error(err)
})



app.listen(port, () => {
    console.log('sever listening at 5000...')
})