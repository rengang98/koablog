module.exports = {
    submit:'Submit',
    errors:{
        moreThan5Times:'Incorrect password for this account multiple times, please try again later.',
        noSure:'unknown mistake, please contact manager'
    },
    resetPass:{
        title:'Reset Password',
        describe:'reset by password',
        password:'privious password',
        resetPass:'new password',
        resetPass2:'confrim new password',
        success:'Password has been reset! please use your new password to login.',
    },
    Navbar:{
        Developers:'Developers',
        Review:' Review ',
        dashboard:' dashboard ',
        resetPass:' resetPass ',
        signUp:' Sign up ',
        logIn:' Log in ',
        Exit:'Exit'
    },
    Signup:{
        title:'Sign Up',
        describe:'Create your DevConnector account',
        submit:"Submit",
        name:"Name",
        email:'Email Address',
        emailInfo:'This site uses Gravatar so if you want a profile image, use a Gravatar email',
        password:'Password',
        password2:'Confirm Password'
    },
    Login:{
        title:'Log in',
        describe:'Use your own account',
        submit:"Submit",
        email:'Email Address',
        password:'Password'
    },
    Profiles:{
        title:'Developers\'s Info',
        ProfileItem:{
            moreInfo:'more info',
            Skills:'Skills'
        }
    },
    Dashboard:{
        title:'Dashboard',
        Welcome:'Welcome',
        remove:'Remove user',
        create:'Create Profile',
        ProfileActive:{
            Edit:'Edit your profile',
            addExp:'Add working experience',
            addEdu:'Add education experience'
        },
        Exprience:{},
        Education:{},
    },
    addExp:{
        back:'back',
        title:'Add working experience',
        describe:'your reference working experience',
        must:'* = must input',
        begin:'* begin from',
        until:'until',
        tillNow:'till now',
        position:'* job position',
        positionInfo:'Your job position',
        company:'* company',
        companyInfo:'company name',
        location:'location',
        locationInfo:'Company Location',
        Program:'working content',
        ProgramInfo:'more refers to working',
    },
    addEdu:{
        back:'back',
        title:'Add education experience',
        describe:'education experience about you...',
        must:'* = must input',
        begin:'* begin from',
        until:'until',
        tillNow:'till now',
        school:'* school',
        degree:'* degree',
        fieldofstudy:'fieldofstudy',
        Program:'Program Description',
        ProgramInfo:'more refers to your education',
    },
    Exprience:{
        title:'Experience',
        company:'company',
        content:'job content',
        time:'working time',
        remove:'remove'
    },
    Education:{
        title:'Education',
        School:'School',
        Degree:'Degree',
        Time:'Time',
        remove:'remove'
    },
    CreateProfile:{
        Back:'Back',
        title:'Create Profile',
        describe:'Please write some Info about yourself',
        must:'* = Must input',
        handle:'* Profile handle',
        handleInfo:'Normally your Email',
        jobs:'* Jobs',
        jobsInfo:'Your job currently',
        company:'company',
        companyInfo:'Your company or where you belongs to',
        website:'website',
        websiteInfo:'company website',
        location:'* address',
        locationInfo:'Your address(city, district .etc)',
        skills:'* Your skillful language',
        skillsInfo:'Please use "," to split (html,css,Ruby)',
        github:'github',
        githubInfo:'Your github',
        bio:'hello! I am ...',
        bioInfo:'Introduce yourself',
        addSocial:'add social account',
        Optional:'Optional',
        Submit:'Submit'
    },
    EditProfile:{
        title:'Edit Profile',
    },
    Profile:{
        title:'personal info',
        about:'About',
        noBio:"This developer didn't write bio",
        skills:'Skills',
        postion:'Position',
        description:'Description',
        experience:'Experience',
        education:'Education',
        degree:'Degree',
        major:'Major',
        github:'Github'
    },
    comment:{
        form:'Say Somthing...',
        back:'Back',
        message:'message him/her',

    }
}