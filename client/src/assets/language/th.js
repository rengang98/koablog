module.exports = {
    submit:"ส่ง",
    errors:{
        moreThan5Times:'รหัสผ่านป้อนผิดหลายครั้ง โปรดลองอีกครั้งในภายหลัง',
        noSure:'ข้อผิดพลาดที่ไม่รู้จักโปรดติดต่อผู้ดูแลระบบ'
    },
    resetPass:{
        title:'รีเซ็ตรหัสผ่าน',
        describe:'แก้ไขด้วยรหัสผ่าน',
        password:'รหัสผ่านเก่า',
        resetPass:'รหัสผ่านใหม่',
        resetPass2:'ยืนยันรหัสผ่านใหม่',
        success:'รีเซ็ตสำเร็จ! กรุณาเข้าสู่ระบบอีกครั้ง',
    },
    Navbar:{
        Developers:'แพลตฟอร์มสำหรับนักพัฒนา',
        Review:' ฟอรัม ',
        dashboard:' แผงข้อมูล ',
        resetPass:' เปลี่ยนรหัสผ่าน ',
        signUp:' ลงทะเบียน ',
        logIn:' เข้าสู่ระบบ ',
        Exit:'ออกจากระบบ'
    },
    Signup:{
        title:'ลงทะเบียน',
        describe:'สร้างแพลตฟอร์มสำหรับนักพัฒนาของคุณเอง',
        submit:"ส่ง",
        name:'ชื่อผู้ใช้',
        email:'อีเมล์',
        emailInfo:'เว็บไซต์นี้ใช้ Gravatar ดังนั้นถ้าคุณต้องการรูปโปรไฟล์โปรดใช้อีเมล Gravatar',
        password:'รหัสผ่าน',
        password2:'ป้อนรหัสผ่านอีกครั้ง'
    },
    Login:{
        title:'เข้าสู่ระบบ',
        describe:'กรุณาเข้าสู่ระบบด้วยบัญชีของคุณ',
        submit:"ส่ง",
        email:'อีเมล์',
        password:'รหัสผ่าน'
    },
    Profiles:{
        title:'ข้อมูลนักพัฒนาทั้งหมดบนแพลตฟอร์ม',
        ProfileItem:{
            moreInfo:'เพิ่มเติม',
            Skills:'ความสามารถ'
        }
    },
    Dashboard:{
        title:'แผงข้อมูล',
        Welcome:'ยินดีต้อนรับ!',
        remove:'ลบข้อมูลผู้ใช้นี้',
        create:'สร้างข้อมูลส่วนบุคคล',
        ProfileActive:{
            Edit:'แก้ไขข้อมูลส่วนบุคคล',
            addExp:'เพิ่มประสบการณ์การทำงาน',
            addEdu:'เพิ่มประสบการณ์การศึกษา'
        },
        Exprience:{},
        Education:{},
    },
    addExp:{
        back:'กลับ',
        title:'เพิ่มประสบการณ์ในการทำงาน',
        describe:'อธิบายประสบการณ์การทำงานของคุณ',
        must:'* = จำเป็น',
        begin:'* วันที่เริ่มต้น',
        until:'วันที่สิ้นสุด',
        tillNow:'จนถึงตอนนี้',
        position:'* ตำแหน่ง',
        positionInfo:'ตำแหน่งงานของคุณ',
        company:'* บริษัท',
        companyInfo:'ชื่อ บริษัท',
        location:'สถานที่',
        locationInfo:'ที่อยู่ บริษัท',
        Program:'ส่วนเสริมเนื้อหางาน',
        ProgramInfo:'ข้อมูลเพิ่มเติมเกี่ยวกับงานของคุณ',
    },
    addEdu:{
        back:'กลับ',
        title:'เพิ่มพูนประสบการณ์ทางการศึกษา',
        describe:'อธิบายประสบการณ์การศึกษา',
        must:'* = จำเป็น',
        begin:'* วันที่เริ่มต้น',
        until:'วันที่สิ้นสุด',
        tillNow:'จนถึงตอนนี้',
        school:'* ชื่อโรงเรียน',
        degree:'* ปริญญา',
        fieldofstudy:'มืออาชีพ',
        Program:'เสริมประสบการณ์การศึกษา',
        ProgramInfo:'เพิ่มเติมเกี่ยวกับประสบการณ์การศึกษาของคุณ',
    },
    Exprience:{
        title:'ประสบการณ์การทำงาน',
        company:'ชื่อ บริษัท',
        content:'เนื้อหางาน',
        time:'ปีทำงาน',
        remove:'ลบ'
    },
    Education:{
        title:'ประสบการณ์การศึกษา',
        School:'ชื่อโรงเรียน',
        Degree:'ปริญญา',
        Time:'ปีการศึกษา',
        remove:'ลบ'
    },
    CreateProfile:{
        Back:'กลับ',
        title:'สร้างข้อมูลส่วนบุคคล',
        describe:'โปรดแนะนำตัวเอง',
        must:'* = จำเป็น',
        handle:'* ชื่อประจำตัว',
        handleInfo:'โดยปกติอีเมลของคุณ',
        jobs:'* งาน',
        jobsInfo:'งานปัจจุบันของคุณ',
        company:'บริษัท',
        companyInfo:'บริษัท ในเครือปัจจุบัน',
        website:'เว็บไซต์',
        websiteInfo:'เว็บไซต์ของ บริษัท',
        location:'* ที่อยู่',
        locationInfo:'ที่อยู่ปัจจุบันของคุณ (ประเทศจังหวัดเมืองถนน ฯลฯ )',
        skills:'* เชี่ยวชาญภาษาคอมพิวเตอร์',
        skillsInfo:'กรุณาคั่นด้วยเครื่องหมายจุลภาคภาษาอังกฤษเช่น: html, css, Ruby',
        github:'github',
        githubInfo:'github ของคุณ',
        bio:'สวัสดี...',
        bioInfo:'โปรดแนะนำตัวเอง',
        addSocial:'เพิ่มข้อมูลโซเชียล',
        Optional:'ไม่บังคับ',
        Submit:'ส่ง'
    },
    EditProfile:{
        title:'แก้ไขข้อมูล',
    },
    Profile:{
        title:'ข้อมูลส่วนบุคคล',
        about:'เกี่ยวกับ',
        noBio:"ผู้พัฒนาไม่ได้เขียนแนะนำตนเองใด ๆ",
        skills:'ทักษะ',
        position:'ตำแหน่ง',
        description:'การอธิบาย',
        experience:'ประสบการณ์การทำงาน',
        education:'ประสบการณ์ทางการศึกษา',
        degree:'ปริญญา',
        major:'สาขาการศึกษา',
        github:'Github'
    },
    comment:{
        form:'เขียนอะไรลงไป...',
        back:'กลับ',
        message:'ฝากข้อความถึงเขา / เธอ',
        
    }
}