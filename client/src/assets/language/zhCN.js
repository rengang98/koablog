module.exports = {
    submit:'提交',
    errors:{
        moreThan5Times:'多次密码错误，请稍后重试',
        noSure:'未知错误，请联系管理员'
    },
    resetPass:{
        title:'重置密码',
        describe:'通过密码修改',
        password:'旧密码',
        resetPass:'新密码',
        resetPass2:'确认新密码',
        success:'重置成功！请重新登录',
    },
    Navbar:{
        Developers:'开发者平台',
        Review:' 论坛 ',
        dashboard:' 信息面板 ',
        resetPass:' 重置密码 ',
        signUp:' 注册 ',
        logIn:' 登录 ',
        Exit:'退出'
    },
    Signup:{
        title:'注册',
        describe:'创建属于您的开发者平台',
        submit:"提交",
        name:'用户名',
        email:'电邮',
        emailInfo:'这个网站使用Gravatar，所以如果你想要一个个人资料图片，使用Gravatar电子邮件',
        password:'密码',
        password2:'确认密码'
    },
    Login:{
        title:'登录',
        describe:'请登录您的账号',
        submit:"提交",
        email:'邮箱',
        password:'密码'
    },
    Profiles:{
        title:'本平台所有开发者信息',
        ProfileItem:{
            moreInfo:'更多',
            Skills:'技能'
        }
    },
    Dashboard:{
        title:'信息面板',
        Welcome:'欢迎！',
        remove:'删除此用户信息',
        create:'创建个人信息',
        ProfileActive:{
            Edit:'编辑个人信息',
            addExp:'添加工作经历',
            addEdu:'添加教育经历'
        },
        Exprience:{},
        Education:{},
    },
    addExp:{
        back:'返回',
        title:'增加工作经历',
        describe:'描述您的工作经历',
        must:'* = 必填',
        begin:'* 开始日期',
        until:'结束日期',
        tillNow:'至今',
        position:'* 职位',
        positionInfo:'您的工作职位',
        company:'* 公司',
        companyInfo:'公司名称',
        location:'地点',
        locationInfo:'公司地址',
        Program:'工作内容补充',
        ProgramInfo:'更多内容关于您的工作',
    },
    addEdu:{
        back:'返回',
        title:'增加教育经历',
        describe:'教育经历描述',
        must:'* = 必填',
        begin:'* 开始日期',
        until:'结束日期',
        tillNow:'至今',
        school:'* 学校名称',
        degree:'* 学位',
        fieldofstudy:'专业',
        Program:'教育经历补充',
        ProgramInfo:'更多内容关于您的教育经历',
    },
    Exprience:{
        title:'工作经历',
        company:'公司名称',
        content:'工作内容',
        time:'工作年限',
        remove:'删除'
    },
    Education:{
        title:'教育经历',
        School:'学校名称',
        Degree:'学位',
        Time:'受教育年限',
        remove:'删除'
    },
    CreateProfile:{
        Back:'返回',
        title:'创建个人信息',
        describe:'请介绍一下自己',
        must:'* = 必填',
        handle:'* 识别名称',
        handleInfo:'通常是您的邮箱',
        jobs:'* 工作',
        jobsInfo:'您目前的工作',
        company:'公司',
        companyInfo:'目前隶属的公司',
        website:'网站',
        websiteInfo:'公司的网址',
        location:'* 住址',
        locationInfo:'您目前的住址（国家，省市，街道等）',
        skills:'* 掌握的计算机语言',
        skillsInfo:'请用英文逗号隔开，如：html,css,Ruby',
        github:'github',
        githubInfo:'您的 github',
        bio:'你们好，我是...',
        bioInfo:'补充介绍一下自己吧',
        addSocial:'增加社交信息',
        Optional:'可选',
        Submit:'提交'
    },
    EditProfile:{
        title:'编辑信息',
    },
    Profile:{
        title:'个人信息',
        about:'关于',
        noBio:"这位开发者尚未写下任何自我介绍",
        skills:'技能',
        position:'职位',
        description:'描述',
        experience:'工作经历',
        education:'教育经历',
        degree:'学位',
        major:'专业',
        github:'Github'
    },
    comment:{
        form:'写下一些什么吧...',
        back:'返回',
        message:'给他/她留言',
        
    }
}