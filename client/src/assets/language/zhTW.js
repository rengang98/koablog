module.exports = {
    submit:'提交',
    errors:{
        moreThan5Times:'多次密碼錯誤，請稍後重試',
        noSure:'未知錯誤，請聯系管理員'
    },
    resetPass:{
        title:'重置密碼',
        describe:'通過密碼修改',
        password:'舊密碼',
        resetPass:'新密碼',
        resetPass2:'確認新密碼',
        success:'重置成功！請重新登錄',
    },
    Navbar:{
        Developers:'開發者平臺',
        Review:' 論壇 ',
        dashboard:' 信息面板 ',
        resetPass:' 重置密碼 ',
        signUp:' 註冊 ',
        logIn:' 登錄 ',
        Exit:'退出'
    },
    Signup:{
        title:'註冊',
        describe:'創建屬於您的開發者平臺',
        submit:"提交",
        name:'用戶名',
        email:'電郵',
        emailInfo:'這個網站使用Gravatar，所以如果妳想要壹個個人資料圖片，使用Gravatar電子郵件',
        password:'密碼',
        password2:'確認密碼'
    },
    Login:{
        title:'登錄',
        describe:'請登錄您的賬號',
        submit:"提交",
        email:'郵箱',
        password:'密碼'
    },
    Profiles:{
        title:'本平臺所有開發者信息',
        ProfileItem:{
            moreInfo:'更多',
            Skills:'技能'
        }
    },
    Dashboard:{
        title:'信息面板',
        Welcome:'歡迎！',
        remove:'刪除此用戶信息',
        create:'創建個人信息',
        ProfileActive:{
            Edit:'編輯個人信息',
            addExp:'添加工作經歷',
            addEdu:'添加教育經歷'
        },
        Exprience:{},
        Education:{},
    },
    addExp:{
        back:'返回',
        title:'增加工作經歷',
        describe:'描述您的工作經歷',
        must:'* = 必填',
        begin:'* 開始日期',
        until:'結束日期',
        tillNow:'至今',
        position:'* 職位',
        positionInfo:'您的工作職位',
        company:'* 公司',
        companyInfo:'公司名稱',
        location:'地點',
        locationInfo:'公司地址',
        Program:'工作內容補充',
        ProgramInfo:'更多內容關於您的工作',
    },
    addEdu:{
        back:'返回',
        title:'增加教育經歷',
        describe:'教育經歷描述',
        must:'* = 必填',
        begin:'* 開始日期',
        until:'結束日期',
        tillNow:'至今',
        school:'* 學校名稱',
        degree:'* 學位',
        fieldofstudy:'專業',
        Program:'教育經歷補充',
        ProgramInfo:'更多內容關於您的教育經歷',
    },
    Exprience:{
        title:'工作經歷',
        company:'公司名稱',
        content:'工作內容',
        time:'工作年限',
        remove:'刪除'
    },
    Education:{
        title:'教育經歷',
        School:'學校名稱',
        Degree:'學位',
        Time:'受教育年限',
        remove:'刪除'
    },
    CreateProfile:{
        Back:'返回',
        title:'創建個人信息',
        describe:'請介紹壹下自己',
        must:'* = 必填',
        handle:'* 識別名稱',
        handleInfo:'通常是您的郵箱',
        jobs:'* 工作',
        jobsInfo:'您目前的工作',
        company:'公司',
        companyInfo:'目前隸屬的公司',
        website:'網站',
        websiteInfo:'公司的網址',
        location:'* 住址',
        locationInfo:'您目前的住址（國家，省市，街道等）',
        skills:'* 掌握的計算機語言',
        skillsInfo:'請用英文逗號隔開，如：html,css,Ruby',
        github:'github',
        githubInfo:'您的 github',
        bio:'妳們好，我是...',
        bioInfo:'補充介紹壹下自己吧',
        addSocial:'增加社交信息',
        Optional:'可選',
        Submit:'提交'
    },
    EditProfile:{
        title:'編輯信息',
    },
    Profile:{
        title:'個人信息',
        about:'關於',
        noBio:"這位開發者尚未寫下任何自我介紹",
        skills:'技能',
        position:'職位',
        description:'描述',
        experience:'工作經歷',
        education:'教育經歷',
        degree:'學位',
        major:'專業',
        github:'Github'
    },
    comment:{
        form:'寫下壹些什麽吧...',
        back:'返回',
        message:'給他/她留言',
        
    }
}