// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from "./router/index";
import axios from "./utils/http";
import store from "./store";

import VueI18n from 'vue-i18n'
import en from './assets/language/en'
import th from './assets/language/th'
import zhCN from './assets/language/zhCN'
import zhTW from './assets/language/zhTW'

Vue.prototype.$axios = axios //全局变量配置

Vue.config.productionTip = false

Vue.use(VueI18n)


const i18n = new VueI18n({
  locale:'en',
  fallbackLocale:'en',
  messages:{
    'en':en,
    'th':th,
    'zhCN':zhCN,
    'zhTW':zhTW,
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  i18n,//必须跟在el之后
  router,
  store,
  components: { App },
  template: '<App/>'
})

