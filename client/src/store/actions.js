export const setIsAuthenticated = ({commit},data)=>{
    commit("setIsAuthenticated",data)
}

export const setUser = ({commit},data) =>{
    commit('setUser',data)
}

export const setProfile = ({commit},data)=>{
    commit('setProfile',data)
}

export const clearCurrentState = ({commit},data)=>{
    commit('setProfile',null)
    commit('setUser',null)
    commit("setIsAuthenticated",false)

}

export const setLoading = ({commit},data) =>{
    commit('setLoading',data)
}

export const setProfiles = ({commit},data)=>{
    commit('setProfiles',data)
}
//针对个人账号输错5次的封停
export const errorTimesPlusOne = ({commit},email)=>{
    commit('errorTimesPlusOne',email)
}
//针对个人账号输错5次的解禁
export const errorTimesClear = ({commit},email)=>{
    commit('errorTimesClear',email)
}

export const errorTimesClearAll = ({commit})=>{
    commit('errorTimesClearAll')
}