import Vue from "vue";
import Vuex from "vuex";
import * as getters from "./getters";
import * as mutations from "./mutations";
import * as actions from "./actions";
//import * as xxx from 'xxx'  会将 "xxx" 中所有 export 导出的内容组合成一个对象返回(或import * as obj from 'xx'  这种写法是把所有的输出包裹到obj对象里);

Vue.use(Vuex)

const state = {
    isAuthenticated:false,
    user:{},
    profile:{},
    profiles:{},//所有开发者的信息
    loading:false,
    errorTimes:{}
}

export default new Vuex.Store({
    state,
    getters:getters,
    mutations:mutations,
    actions:actions
})