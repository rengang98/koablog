export const setIsAuthenticated = (state, data) => {
    state.isAuthenticated = data
}

export const setUser = (state, data) => {
    state.user = data
}

export const setProfile = (state, data) => {
    state.profile = data
}

export const setLoading = (state, data) => {
    state.loading = data
}

export const setProfiles = (state, data) =>{
    state.profile = data
}
//针对个人账号输错5次的封停
export const errorTimesPlusOne = (state,email) =>{
    if(!state.errorTimes[email]) state.errorTimes[email] = 1
    else state.errorTimes[email]+=1
}
//针对个人账号输错5次的解禁
export const errorTimesClear = (state,email) =>{
    state.errorTimes[email] = 0
}
//定期清空封停名单
export const errorTimesClearAll = (state) =>{
    state.errorTimes = {}
}