const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PostSchema = new Schema({
    user:{
        type:String,
        ref:'users',
        required:true
    },
    text:{
        type:String,
        required:true
    },
    name:{
        type:String
    },
    likes:[
        {
            user:{
                type:Schema.Types.ObjectId,//指定一个ObjectId类型，使用Schema.Types
                                            //外侧user定义type为String暂时理解为，外侧在拼JSON
                                            //的时候是人为赋值 即user:ctx.state.user.id
                                            //另一种个人理解：是这里的user就是
                ref:'users'
            }
        }
    ],
    comments:[
        {
            user:{
                type:Schema.Types.ObjectId,
                ref:'users'
            },
            name:{
                type:String,
                required:true
            },
            avatar:{
                type:String,
            },
            text:{
                type:String,
                required:true
            },
        }
    ],
    avatar:{
        type:String
    },
    date:{
        type:Date,
        default:Date.now
    }

})

module.exports = Post = mongoose.model('posts',PostSchema)