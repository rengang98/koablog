const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ProfileSchema = new Schema({
    user: {//关联User表
        type: String,
        ref: 'users',
        required: true
    },
    handle: {
        type: String,
        required: true,
        max: 40
    },
    company: {
        type: String,
    },
    website: {
        type: String,
    },
    location: {
        type: String,
    },
    status: {
        type: String,
        required: true
    },
    skills: {
        type: [String],//类型数组，元素类型字符串
        required: true
    },
    bio: {
        type: String,
    },
    github: {
        type: String,
    },
    experience: [ //类型数组，元素类型是对象
        {
            current:{
                type:Boolean,
                default:true
            },
            title:{
                type:String,
                required:true
            },
            company:{
                type:String,
                required:true
            },
            location:{
                type:String,
            },
            from:{
                type:String,
                required:true
            },
            to:{
                type:String,
            },
            description:{
                type:String,
            }

        }
    ],
    education: [ //类型数组，元素类型是对象
        {
            current:{
                type:Boolean,
                default:true
            },
            school:{
                type:String,
                required:true
            },
            degree:{
                type:String,
                required:true
            },
            fieldofstudy:{
                type:String,
            },
            from:{
                type:String,
                required:true
            },
            to:{
                type:String,
            },
            description:{
                type:String,
            }
        }
    ],
    social:{
        wechat:{
            type:String,
        },
        QQ:{
            type:String,
        },
        line:{
            type:String,
        },
        whatsapp:{
            type:String,
        }
    },
    date: {
        type: Date,
        default:
        Date.now
    }
})

module.exports = Profile = mongoose.model('profile',ProfileSchema)