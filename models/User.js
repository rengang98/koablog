const mongoose = require('mongoose')
const Schema = mongoose.Schema

//实例化数据模版
const UserSchema = new Schema({
    name:{
        type:String,
        required:true//字段必须有
    },
    email:{
        type:String,
        required:true//字段必须有
    },
    password:{
        type:String,
        required:true//字段必须有
    },
    avatar:{
        type:String,
    },
    date:{
        type:Date,
        default:Date.now()
    },
})

module.exports = User = mongoose.model('users',UserSchema)