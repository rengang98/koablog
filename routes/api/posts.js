const Router = require('koa-router')
const router = new Router()
const passport = require('koa-passport')
const Post = require('../../models/Post')
const validatePost = require('../../validation/post')
const validateComment = require('../../validation/comment')
const Profile = require('../../models/Profiles')
const post = require('../../validation/post')
const { default: validator } = require('validator')
const { remove } = require('../../models/Post')

/**
 * @route POST api/posts/
 * @desc post review
 * @access private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async ctx => {
    //先确认该用户有profile
    const profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let { errors, isValid } = validatePost(ctx.request.body)
        if (!isValid) {
            ctx.status = 404
            ctx.body = errors
            return
        }

        const newPost = new Post({
            text: ctx.request.body.text,
            name: ctx.request.body.name,
            avatar: ctx.request.body.avatar,
            user: ctx.state.user.id
        })
        await newPost.save().then(post => {
            ctx.body = post
        }).catch(err => {
            ctx.body = { error: err }
        })

        ctx.body = newPost
    } else {
        ctx.status = 404
        ctx.body = { error: 'You have to complete profile first' }
    }
})

/**
 * @route GET api/posts/all
 * @desc get all review
 * @access public
 */

router.get('/all', async ctx => {
    await Post.find().sort({ date: -1 }).then(posts => {
        ctx.status = 200
        ctx.body = posts
    }).catch(err => {
        ctx.status = 404
        ctx.body = { nopost: "there is no any Post" }
    })
})

/**
* @route GET api/posts?post_id=xxx
* @desc get single review
* @access public
*/
router.get('/', async ctx => {
    const id = ctx.query.post_id
    await Post.findById(id).then(post => {
        ctx.status = 200
        ctx.body = post
    }).catch(err => {
        ctx.status = 404
        ctx.body = { nopost: "no Post found" }
    })
})

/**
* @route DELETE api/posts?post_id=xxx
* @desc delete single review
* @access private
*/

router.delete('/', passport.authenticate('jwt', { session: false }), async ctx => {
    //先确认该用户有profile
    const profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let post
        try {
            post = await Post.findById(ctx.query.post_id)
        } catch (err) {
            ctx.status = 404
            ctx.body = { nopost: "no Post found" }
            return
        }
        //用户只能删除自己的留言
        if (post.user.toString() !== ctx.state.user.id) {
            ctx.status = 401
            ctx.body = { unauthenticate: 'user illegal request!' }
            return
        }
        await Post.deleteOne({ _id: ctx.query.post_id }).then(dltRlt => {
            ctx.status = 200
            ctx.body = { ok: dltRlt.ok }
        }).catch(err => {
            ctx.status = 404
            ctx.body = { nopost: "no Post found" }
        })
    } else {
        ctx.status = 404
        ctx.body = { error: 'no profile of this user be found' }
    }
})


/**
 * @route api/posts/like?post_id=xxx
 * @desc add like
 * @access private
 */

router.post('/like', passport.authenticate('jwt', { session: false }), async ctx => {
    //点赞者先要有自己的profile 
    const profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let post
        try {
            post = await Post.findById(ctx.query.post_id)
        } catch (err) {
            ctx.status = 404
            ctx.body = { nopost: "no Post found" }
            return
        }
        //判断是否已经点赞了
        const isLike = post.likes.filter(like => like.user.toString() === ctx.state.user.id).length > 0
        if (isLike) {
            ctx.status = 400
            ctx.body = { alredayLiked: 'current user already liked it' }
            return
        }
        post.likes.unshift({ user: ctx.state.user.id })
        const postUpdate = await Post.findOneAndUpdate({ _id: ctx.query.post_id },
            { $set: post },
            {
                new: true,
                useFindAndModify: false
            }
        )
        ctx.status = 200
        ctx.body = postUpdate
    } else {
        ctx.status = 404
        ctx.body = { error: 'no profile of this user be found' }
    }
})

/**
 * @route api/posts/deleteLike?post_id=xxx
 * @desc delete like
 * @access private
 */
router.delete('/deleteLike', passport.authenticate('jwt', { session: false }), async ctx => {
    const profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        const post = await Post.findById(ctx.query.post_id)
        //判断是否尚未点赞
        const unLike = post.likes.filter(like => like.user.toString() === ctx.state.user.id).length == 0
        if (unLike) {
            ctx.status = 400
            ctx.body = { alredayLiked: 'you haven\'t liked it' }
            return
        }
        const removeIndex = post.likes.map(like => like.user.toString()).indexOf(ctx.state.user.id)
        post.likes.splice(removeIndex, 1)

        const postUpdate = await Post.findOneAndUpdate({ _id: ctx.query.post_id },
            { $set: post },
            { new: true }
        )
        ctx.status = 200
        ctx.body = postUpdate
    } else {
        ctx.status = 404
        ctx.body = { error: 'no profile of this user be found' }
    }

})

/**
 * @route GET api/posts/checkliked/post_id=xxx
 * @desc check current user already liked the post or not
 * @access private
 */

router.get('/checkliked', passport.authenticate('jwt', { session: false }), async ctx => {
    //先确认当前用户有profile
    let profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        //找post
        let post
        try {
            post = await Post.findById(ctx.query.post_id)
        } catch (error) {
            ctx.status = 404
            ctx.body = { error: 'no such post found' }
        }
        let alredayLiked = post.likes.map(like => like.user).indexOf(ctx.state.user.id) != -1
        ctx.status = 200
        ctx.body = { alredayLiked: alredayLiked }
    } else {
        ctx.status = 404
        ctx.body = { error: 'no profile of this user be found' }
    }
})

/**
 * @route POST api/posts/addcomment/post_id=xxx
 * @desc add comment
 * @access private
 */
router.post('/addcomment', passport.authenticate('jwt', { session: false }), async ctx => {
    //先确定用户有profile
    let profile = await Profile.find({ user: ctx.state.user.id }).populate('user', ['name', 'avatar'])
    if (profile.length) {
        //定位相关Post
        let post
        try {
            post = await Post.findById(ctx.query.post_id)
        } catch (error) {
            ctx.status = 404
            ctx.body = { noprofile: "no such post found" }
            return
        }
        //验证输入
        let { errors, isValid } = validateComment(ctx.request.body)
        if (!isValid) {
            ctx.status = 404
            ctx.body = errors
            return
        }

        post.comments.unshift({
            user: ctx.state.user.id,
            name: profile[0].user.name,
            avatar: profile[0].user.avatar,
            text: ctx.request.body.text,
        })
        let postUpadte = await Post.findOneAndUpdate({ _id: ctx.query.post_id },
            { $set: post },
            { new: true })
        ctx.status = 200
        ctx.body = postUpadte
    } else {
        ctx.status = 404
        ctx.body = { noprofile: "this user has no profile" }
    }
})

/**
 * @route DELETE api/posts/deletecomment?post_id=xxx&comment_id==xxx
 * @desc delete comment from post
 * @access private
 */

router.delete('/deletecomment', passport.authenticate('jwt', { session: false }), async ctx => {
    //先确认当前用户有profile
    let profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let post
        try {
            post = await Post.findById(ctx.query.post_id)
        } catch (err) {
            ctx.status = 404
            ctx.body = { noprofile: 'no such post found' }
            return
        }
        //post下的所有留言遍历找到对应留言,并判断要删除的留言是否是当前用户的留言
        let removeIndex = post.comments.map(item => item.id).indexOf(ctx.query.comment_id)
        if (removeIndex == -1) {
            ctx.status = 404
            ctx.body = { noprofile: 'comment not exist' }
            return
        }

        let commentBelongstoUser = post.comments[removeIndex].user.toString() === ctx.state.user.id
        if (!commentBelongstoUser) {
            ctx.status = 404
            ctx.body = { noprofile: 'user illegal request!' }
            return
        }
        post.comments.splice(removeIndex, 1)
        let postUpadte = await Post.findOneAndUpdate({ _id: ctx.query.post_id },
            { $set: post },
            { new: true })
        ctx.status = 200
        ctx.body = postUpadte

    } else {
        ctx.status = 404
        ctx.body = { noprofile: 'current user has no profile' }
    }
})

/**
 * @route test
 */
router.get('/test', async ctx => {
    console.log('post interface worked...');
})

module.exports = posts = router.routes()