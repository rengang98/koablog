const Router = require('koa-router')
const router = new Router()
const passport = require('koa-passport')
//引入模版实例
const Profile = require('../../models/Profiles')
const User = require('../../models/User')
//引入验证
const validateProfileInput = require('../../validation/profile')
const validateExperience = require('../../validation/experience')
const validateEducation = require('../../validation/education')
const { default: validator } = require('validator')
const { createIndexes } = require('../../models/Profiles')
/**
 * @route GET api/profile
 * @desc user profile interface
 * @access interface is private
 */
router.get('/', passport.authenticate('jwt', { session: false }), async ctx => {
    //根据已登录用户拥有的id 找该用户的profile 然后取其中的name和avatar,populate负责跨表取数据
    let profile = await Profile.findOne({ user: ctx.state.user.id },{useFindAndModify:false})
        .populate('user', ['name', 'avatar'])
    if (profile) {
        ctx.status = 200
        ctx.body = profile
    } else {
        ctx.status = 404
        ctx.body = { noprofile: 'This user has no any profile' }

    }
})

/**
 * @route POST api/profile
 * @desc add/edit profile interface
 * @access private
 */
router.post('/', passport.authenticate('jwt', { session: false }), async ctx => {
    //验证输入先
    const { errors, isValid } = validateProfileInput(ctx.request.body)
    if (!isValid) {
        ctx.status = 400
        ctx.body = errors
        return
    }

    const profileFeilds = {}
    profileFeilds.user = ctx.state.user.id

    if (ctx.request.body.handle) {
        profileFeilds.handle = ctx.request.body.handle
    }
    if (ctx.request.body.company) {
        profileFeilds.company = ctx.request.body.company
    }
    if (ctx.request.body.website) {
        profileFeilds.website = ctx.request.body.website
    }
    if (ctx.request.body.location) {
        profileFeilds.location = ctx.request.body.location
    }
    if (ctx.request.body.status) {
        profileFeilds.status = ctx.request.body.status
    }
    //skills 转成数组 前端传来字符串比如 "html,云计算,css,vue"
    if (typeof ctx.request.body.skills !== undefined) {
        profileFeilds.skills = ctx.request.body.skills.split(',')
    }

    if (ctx.request.body.bio) {
        profileFeilds.bio = ctx.request.body.bio
    }
    if (ctx.request.body.github) {
        profileFeilds.github = ctx.request.body.github
    }

    profileFeilds.social = {}

    if (ctx.request.body.wechat) {
        profileFeilds.social.wechat = ctx.request.body.wechat
    }
    if (ctx.request.body.QQ) {
        profileFeilds.social.QQ = ctx.request.body.QQ
    }
    if (ctx.request.body.line) {
        profileFeilds.social.line = ctx.request.body.line
    }
    if (ctx.request.body.whatsapp) {
        profileFeilds.social.whatsapp = ctx.request.body.whatsapp
    }

    //查询数据库 决定是添加还是修改
    let profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length > 0) {
        let profileUpdate = await Profile.findOneAndUpdate(
            { user: ctx.state.user.id },
            { $set: profileFeilds },
            {
                new: true, //需要增加一个配置项 { new: true }，则可以返回更新后的内容。
                useFindAndModify: false
            },
        )
        ctx.body = profileUpdate
    } else {
        let newProfile = new Profile(profileFeilds)
        await newProfile.save().then(profile => {
            ctx.status = 200
            ctx.body = profile
        })
        // await new Profile(profileFeilds).save().then(profile=>{
        //     ctx.status = 200
        //     ctx.body = profile
        // })
    }

})

/**
 * @route GET api/profile/handle?handle=xxx
 * @desc get profile by handle
 * @access public 
 */
router.get('/handle', async ctx => {
    const errors = {}
    const handle = ctx.query.handle
    let profile = await Profile.find({ handle: handle })
        .populate('user', ['name', 'avatar'])
    // console.log(profile)
    if (!profile.length) {
        errors.npprofile = 'Didn\'t find user\'s profile!'
        ctx.status = 404
        ctx.body = errors
    } else {
        ctx.status = 200
        ctx.body = profile[0]
    }
})

/**
 * @route GET api/profile/user_id?user_id=xxx
 * @desc get profile by user_id
 * @access public
 */

router.get('/user_id', async ctx => {
    let errors = {}
    let user_id = ctx.query.user_id
    let profile = await Profile.find({ user: user_id })
        .populate('user', ['name', 'avatar'])
    if (!profile.length) {
        errors.noprofile = 'Didn\'t find user\'s profile!'
        ctx.status = 404
        ctx.body = errors
    } else {
        ctx.status = 200
        ctx.body = profile[0]
    }
})

/**
 * @route GET api/profile/all
 * @desc get all users' profiles 
 * @access public
 */

router.get('/all', async ctx => {
    const errors = {}
    let profileAll = await Profile.find({}).populate('user', ['name', 'avatar'])
    if (!profileAll.length) {
        errors.noprofile = 'no users\' profiles'
        ctx.status = 404
        ctx.body = errors
    } else {
        ctx.status = 200
        ctx.body = profileAll
    }
})

/**
 * @route POST api/profile/experience
 * @desc add user's experience 
 * @access private
 */

router.post('/experience', passport.authenticate('jwt', { session: false }), async ctx => {

    let { errors, isValid } = validateExperience(ctx.request.body)
    if (!isValid) {
        ctx.status = 404
        ctx.body = errors
        return
    }

    let profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let newExp = {
            current: ctx.request.body.current,
            title: ctx.request.body.title,
            company: ctx.request.body.company,
            location: ctx.request.body.location,
            from: ctx.request.body.from,
            to: ctx.request.body.to,
            description: ctx.request.body.description
        }

        // experience.unshift(newExp)
        let profileUpdate = await Profile.findOneAndUpdate(
            { user: ctx.state.user.id },
            { $addToSet: { experience: newExp } },//还有另一种方式，就是用$push替换$addToSet，也能达到添加元素的效果，那么，这两种方式有什么区别呢？
            //$addToSet：向数组中添加元素，若数组本身含有该元素，则不添加，否则，添加，这样就避免了数组中的元素重复现象；
            //$push：向数组尾部添加元素，但它不管数组中有没有该元素，都会添加。
            { new: true })

        ctx.status = 200
        ctx.body = profileUpdate
    } else {
        errors.noprofile = "Didn\'t find user\'s profile!"
        ctx.status = 404
        ctx.body = errors
    }
})

/**
 * @route DELETE api/profile/experience/?exp_id=xxx
 * @desc delete working experience
 * @access private
 */

router.delete('/experience', passport.authenticate('jwt', { session: false }), async ctx => {
    const exp_id = ctx.query.exp_id
    let errors = {};
    if (!exp_id) {
        errors.noExpId = "exp_id is required"
        ctx.status = 404
        ctx.body = errors
        return
    }
    //查询

    const profile = await Profile.findOne({ user: ctx.state.user.id })
    if (profile) {
        // let findRlt = await profile.find()
        let removeIndex = profile.experience.map(item => { return item.id }).indexOf(exp_id)
        if (removeIndex == -1) {
            errors.noExpId = "exp_id not exist"
            ctx.status = 404
            ctx.body = errors
            return
        }
        profile.experience.splice(removeIndex, 1)
        // //更新
        let profileUpdate = await Profile.findOneAndUpdate({ user: ctx.state.user.id }, { $set: profile }, { new: true })

        ctx.status = 200
        ctx.body = profileUpdate
    } else {
        errors.noprofile = "Didn\'t find user\'s profile!"
        ctx.status = 404
        ctx.body = errors
    }
})

/**
 * @route POST api/profile/education
 * @desc add user's education 
 * @access private
 */

router.post('/education', passport.authenticate('jwt', { session: false }), async ctx => {
    let { errors, isValid } = validateEducation(ctx.request.body)
    if (!isValid) {
        ctx.status = 404
        ctx.body = errors;
        return
    }

    let newEdu = {
        current: ctx.request.body.current,
        school: ctx.request.body.school,
        degree: ctx.request.body.degree,
        fieldofstudy: ctx.request.body.fieldofstudy,
        from: ctx.request.body.from,
        to: ctx.request.body.to,
        description: ctx.request.body.description,
    }
    let profile = await Profile.find({ user: ctx.state.user.id })
    if (profile.length) {
        let profileUpate = await Profile.findOneAndUpdate(
            { user: ctx.state.user.id },
            { $addToSet: { education: newEdu } },
            { new: true }
        )

        ctx.status = 200
        ctx.body = profileUpate
    } else {
        errors.noprofile = "Didn\'t find user\'s profile!"
        ctx.status = 404
        ctx.body = errors
    }
})

/**
 * @route DELETE api/profile/education?edu_id=xxx
 * @desc delete education
 * @access private
 */

router.delete('/education', passport.authenticate('jwt', { session: false }), async ctx => {
    let edu_id = ctx.query.edu_id
    let errors = {}
    if (!edu_id) {
        errors.noExpId = "edu_id is required"
        ctx.status = 404
        ctx.body = errors
        return
    }
    const profile = await Profile.findOne({ user: ctx.state.user.id })
    if (profile) {
        let removeIndex = profile.education.map(item => item.id).indexOf(edu_id)
        if (removeIndex == -1) {
            errors.noExpId = "edu_id not exist"
            ctx.status = 404
            ctx.body = errors
            return
        }
        profile.education.splice(removeIndex, 1)
        let profileUpdate = await Profile.findOneAndUpdate({ user: ctx.state.user.id }, { $set: profile }, { new: true })
        ctx.status = 200
        ctx.body = profileUpdate
    } else {
        errors.noprofile = "Didn\'t find user\'s profile!"
        ctx.status = 404
        ctx.bdoy = errors
    }
})

/**
 * @route DELETE api/profile/ 
 * @desc delete user 用于在用户profile页设置一个删除用户连接
 * @access private
 */

router.delete('/', passport.authenticate("jwt", { session: false }), async ctx => {
    const profile = await Profile.deleteOne({ user: ctx.state.user.id })
    if (profile.ok == 1) {
        const user = await User.deleteOne({ _id: ctx.state.user.id })
        if (user.ok == 1) {
            ctx.status = 200
            ctx.body = { success: true }
        }
    } else {
        ctx.state = 404
        ctx.body = { noprofile: 'profile not exist' }
    }
})


//test
router.get('/test', async ctx => {
    ctx.status = 200
    ctx.body = { msg: "profile api is works..." }
})

module.exports = router.routes()