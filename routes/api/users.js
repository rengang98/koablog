const Router = require('koa-router')
const router = new Router()
const tools = require('../../config/tools')
const bcrypt = require('bcryptjs')
//生成token的辅助npm
const jwt = require('jsonwebtoken')
const keys = require('../../config/keys')
//验证token的辅助npm
const passport = require('koa-passport')
//全球公用头像
const gravatar = require('gravatar')
//引入User
const User = require('../../models/User')
//引入input验证
const validateRegisterInput = require("../../validation/register")
const validateLoginInput = require("../../validation/login")
const validateResetPass = require("../../validation/resetPass")
const { createTestAccount } = require('nodemailer')

/**
 * @route Post api/users/register
 * @desc register interface
 * @access public
 */
router.post('/register', async ctx => {
    try {

        //验证输入
        const { errors, isValid } = validateRegisterInput(ctx.request.body)
        if (!isValid) {
            ctx.status = 400
            ctx.body = errors
            return
        }

        let email = ctx.request.body.email
        let rltEmail = await User.find({ email: email })
        if (rltEmail.length > 0) {
            ctx.status = 500
            ctx.body = { email: 'Email already exist!' }
        } else {
            const avatar = gravatar.url(ctx.request.body.email, { s: '200', r: 'pg', d: 'mm' })
            const newUser = new User({
                name: ctx.request.body.name,
                email: ctx.request.body.email,
                password: tools.enbcrypt(ctx.request.body.password),
                avatar: 'https:' + avatar
            })
            await newUser.save()
            ctx.body = newUser

            //回调导致无法顺序同步执行，原因尚未知
            // await bcrypt.genSalt(10, (err, salt) => {
            //     // if(err) throw err
            //     bcrypt.hash(newUser.password, salt, (err, hash) => {
            //         if (err) throw err
            //         newUser.password = hash
            //     })
            // })

            // await newUser.save().then(user => {
            //     ctx.body = user
            // }).catch(err => {
            //     console.log(err)
            // })
            // ctx.body = newUser
        }
    } catch (err) {
        console.error(err)
        ctx.body = { err: err.message }
    }
})

/**
 * @route Post api/users/login
 * @desc login interface, return token
 * @access public
 */
router.post('/login', async ctx => {
    try {
        let { errors, isValid } = validateLoginInput(ctx.request.body)
        if (!isValid) {
            ctx.status = 400
            ctx.body = errors
            return
        }
        let email = ctx.request.body.email
        let password = ctx.request.body.password
        if (!email) ctx.status = 404, ctx.body = { email: 'email cannot be empty！' }
        else if (!password) ctx.status = 404, ctx.body = { password: 'password cannot be empty！！' }
        else {
            let rltEmail = await User.find({ email: email })
            if (!rltEmail.length) ctx.status = 404, ctx.body = { email: 'email not exist' }
            else {
                let user = rltEmail[0]
                //bcrypt.compareSync方法直接比较输入密码和加密后的密码，无需加入中间值salt
                let result = await bcrypt.compareSync(ctx.request.body.password, user.password)
                if (result) {
                    //返回token
                    let payload = {
                        id: user.id,
                        name: user.name,
                        avatar: user.avatar
                    }
                    let token = jwt.sign(payload, keys.secretOrKey, { expiresIn: 60 * 60 })
                    ctx.status = 200
                    /**注意：
                     token一般是在请求头里加入Authorization，并加上Bearer标注：*/
                    ctx.body = { success: true, token: 'Bearer ' + token }
                } else {
                    ctx.status = 404
                    ctx.body = { password: 'password wrong！' }
                }
            }
        }
    } catch (err) {
        console.error(err)
        ctx.body = { err: err.message }
    }
})

/**
 * @route Post api/users/resetPass
 * @desc reset user's password by old password
 * @access private
 */

router.post('/resetPass', passport.authenticate('jwt', { session: false }), async ctx => {
    try {
        let { errors, isValid } = validateResetPass(ctx.request.body)
        if (!isValid) {
            ctx.status = 404
            ctx.body = errors
            return
        }
        let password = ctx.request.body.password
        let resetPass = ctx.request.body.resetPass
        if (!password) ctx.status = 404, ctx.body = { email: 'password cannot be empty！' }
        if (!resetPass) ctx.resetPass = 404, ctx.body = { resetPass: 'password cannot be empty！！' }
        else {
            let rlt = await User.findById(ctx.state.user.id)
            if(bcrypt.compareSync(password,rlt.password)){
                if(password == resetPass) return ctx.status = 404, ctx.body = {resetPass:'new password cannot be same as previous one'}
                rlt.password = tools.enbcrypt(resetPass)
                await User.updateOne({_id:ctx.state.user.id},rlt).catch(err=>ctx.body = { err: "database mistake, please contact manager" })
                ctx.status = 200
                ctx.body = {
                    success:true,
                    msg:'password reseted'
                }
            }else{
                ctx.status = 404
                ctx.body = { password: 'password wrong！' }
            }
        }
    } catch (err) {
        console.error(err);
        ctx.body = { err: err.message }
    }
})

/**
 * @route Get api/users/current
 * @desc userInfo interface , return userInfo
 * @access private
 */
router.get('/current', passport.authenticate('jwt', { session: false }), async ctx => {
    ctx.body = {
        success: true,
        body: {
            id: ctx.state.user.id,
            name: ctx.state.user.name,
            email: ctx.state.user.email,
            avatar: ctx.state.user.avatar,
        } //由passport中done(null,user)传回
    }
})

//test
router.get('/test', async ctx => {
    ctx.status = 200
    ctx.body = { msg: 'test works...' }
})

module.exports = router.routes()//当前route定义下的所有的get以及post等请求