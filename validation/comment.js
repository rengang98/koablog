const validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateCommentIput = data => {
    let errors = {}
    data.text = !isEmpty(data.text) ? data.text : ''
    if(validator.isEmpty(data.text)){
        errors.text = "comment content cannot be empty!"
    }
    if(!validator.isLength(data.text,{min:0,max:500})){
        errors.text = "the length of comment content should between 0 to 500"
    }

    return{
        errors,
        isValid:isEmpty(errors)
    }
}