const validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateEducation = data => {
    const errors = {}
    !isEmpty(data.school) ? data.school : ''
    !isEmpty(data.degree) ? data.degree : ''
    !isEmpty(data.from) ? data.from : ''

    if (validator.isEmpty(data.school)) {
        errors.school = "school cannot be empty"
    }
    if (validator.isEmpty(data.degree)) {
        errors.degree = "degree cannot be empty"
    }
    if (validator.isEmpty(data.from)) {
        errors.from = "from cannot be empty"
    }

    return{
        errors:errors,
        isValid:isEmpty(errors)
    }

}