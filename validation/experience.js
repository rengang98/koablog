const validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateExperience = data => {
    const errors = {}
    !isEmpty(data.title) ? data.title : ''
    !isEmpty(data.company) ? data.company : ''
    !isEmpty(data.from) ? data.from : ''

    if (validator.isEmpty(data.title)) {
        errors.title = "title cannot be empty"
    }
    if (validator.isEmpty(data.company)) {
        errors.company = "company cannot be empty"
    }
    if (validator.isEmpty(data.from)) {
        errors.from = "from cannot be empty"
    }

    return{
        errors:errors,
        isValid:isEmpty(errors)
    }

}