const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateLoginInput = (data) => {
    let errors = {}

    data.email = !isEmpty(data.email)? data.email:''
    data.password = !isEmpty(data.password)? data.password:''

    if(Validator.isEmpty(data.email)){
        errors.email = "email cannot be empty !"
    }
    if(Validator.isEmpty(data.password)){
        errors.password = "password cannot be empty !"
    }
    if(!Validator.isEmail(data.email)){
        errors.email = "email is illegal !"
    }


    if(!Validator.isLength(data.password,{min:6,max:30})){
        errors.password = "Length of passpword should between 6 to 30 !"
    }


    return {
        errors,
        isValid:isEmpty(errors)
    }
}