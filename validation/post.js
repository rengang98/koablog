const validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validatePostInput = data => {
    const errors = {}
    data.text = !isEmpty(data.text) ? data.text : ""
    // data.name = !isEmpty(data.name) ? data.name : ""

    if (validator.isEmpty(data.text)) {
        errors.text = "text cannot be empty"

    }

    if (!validator.isLength(data.text, { min: 10, max: 300 })) {
        errors.text = "the length of test should between 10 to 300!"
    }

    return { errors, isValid: isEmpty(errors) }
}