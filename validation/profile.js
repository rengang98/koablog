const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateProfileInput = (data) => {
    let errors = {}
    //防止undefined传入Validator.isEmpty,因为其不接受undefined，会报错
    data.handle = !isEmpty(data.handle) ? data.handle : ''
    data.status = !isEmpty(data.status) ? data.status : ''
    data.skills = !isEmpty(data.skills) ? data.skills : ''
    data.website = !isEmpty(data.website) ? data.website : ''
    
    if (Validator.isEmpty(data.handle)) {
        errors.handle = "handle cannot be empty !"
    }

    if (Validator.isEmpty(data.status)) {
        errors.status = "status cannot be empty !"
    }

    if (Validator.isEmpty(data.skills)) {
        errors.skills = "skills cannot be empty !"
    }

    if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
        errors.handle = "length of handle should between 2 to 40"
    }


    
    if (!Validator.isEmpty(data.website)) {
        if (!Validator.isURL(data.website)) {
            errors.website = "url not legal"
        }
    }

    return {
        errors,
        isValid: isEmpty(errors)
    }
}