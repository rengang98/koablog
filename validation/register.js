const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateRegisterInput = (data) => {

    let errors = {}
    data.name = !isEmpty(data.name)? data.name:''
    data.email = !isEmpty(data.email)? data.email:''
    data.password = !isEmpty(data.password)? data.password:''
    data.password2 = !isEmpty(data.password2)? data.password2:''
    
    if(Validator.isEmpty(data.name)){
        errors.name = "name cannot be empty !"
    }
    if(Validator.isEmpty(data.email)){
        errors.email = "email cannot be empty !"
    }
    if(Validator.isEmpty(data.password)){
        errors.password = "password cannot be empty !"
    }
    if(!Validator.isEmail(data.email)){
        errors.email = "email is illegal !"
    }

    if(!Validator.isLength(data.name,{min:2,max:30})){
        errors.name = "Length of name should between 2 to 30 !"
    }

    if(!Validator.isLength(data.password,{min:6,max:30})){
        errors.password = "Length of passpword should between 6 to 30 !"
    }

    if(Validator.isEmpty(data.password2)){
        errors.password2 = "password 2 cannot be empty !"
    }

    if(!Validator.equals(data.password,data.password2)){
        errors.password = "2 times input of password are not same !"
    }

    return {
        errors,
        isValid:isEmpty(errors)
    }
}