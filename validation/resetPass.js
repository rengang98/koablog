const Validator = require('validator')
const isEmpty = require('./isEmpty')

module.exports = validateResetPass = (data)=>{
    let errors = {}
    data.resetPass = !isEmpty(data.resetPass)?data.resetPass:''
    data.resetPass2 = !isEmpty(data.resetPass2)?data.resetPass2:''

    if(Validator.isEmpty(data.password)){
        errors.password = "password cannot be empty !"
    }

    if(!Validator.isLength(data.password,{min:6,max:30})){
        errors.password = "Length of passpword should between 6 to 30 !"
    }

    if(Validator.isEmpty(data.resetPass)){
        errors.name = "new password cannot be empty !"
    }
    if(!Validator.isLength(data.resetPass,{min:6,max:30})){
        errors.resetPass = "Length of new passpword should between 6 to 30 !"
    }
    if(!Validator.equals(data.resetPass,data.resetPass2)){
        errors.resetPass = "2 times input of new password are not same !"
    }
    if(!data.resetPass.match(/^(?![0-9]+$)(?![a-zA-Z]+$)[0-9A-Za-z]{0,}$/)){
        errors.resetPass = '"password should include at least One letter and One number"'
    }

    return {
        errors,
        isValid:isEmpty(errors)
    }
}